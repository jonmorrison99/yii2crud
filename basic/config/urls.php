<?php
/**
 * Created by PhpStorm.
 * User: dell-laptop
 * Date: 12/15/2015
 * Time: 5:57 PM
 */

return [
    'mycategory/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

];