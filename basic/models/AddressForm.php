<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

class AddressForm extends Model
{
    public $id;
    public $fname;
    public $lname;
    public $dob;
    public $zip;

    /**
     * Return an array of rules to validate the form against
     * @return array
     */
    public function rules()
    {
        return [
            [['fname', 'lname', 'dob', 'zip'], 'required'],
            ['id', 'number'],
            ['dob', 'date', 'format' => 'M/d/yyyy'],
            ['fname', 'string', 'length' => [3,30]],
            ['lname', 'string', 'length' => [3,30]],
            ['zip', 'validateZip'],
        ];
    }

    /**
     * should validate the zip for 5 digits. Not working
     */
    public function validateZip() {
        if ((strlen($this->zip) == 5 ) && is_numeric($this->zip)) { }
        else {
            $this->addError('zip', 'Zip should be 5 digits');
        }
    }

    /**
     * Convert date into mysql format
     * @param $old
     * @return mixed
     */
    public function dateFormat($old, $pattern='Y-m-d') {
        return (new \DateTime($old))->format($pattern);
    }

    /**
     * Validates and saves the contact to the database using prepared statements
     * @param $params
     */
    public function save($params) {
        $connection = Yii::$app->db;

        $connection->createCommand(
            "insert into addressbook (first_name, last_name, dob, zip) values (:first_name, :last_name, :dob, :zip)"
                )->bindValues([
                    ':first_name' => trim($params['fname']),
                    ':last_name' => trim($params['lname']),
                    ':dob' => $this->dateFormat($params['dob']),
                    ':zip' => $params['zip'],
                ])->execute();
    }

    /**
     * drop item from database using prepared statements
     * @param $params
     * @throws \yii\db\Exception
     */
    public function deleteOne($params) {
        $connection = Yii::$app->db;

        $connection->createCommand(
            "delete from addressbook where id=:id"
        )
            ->bindValues([
                ':id' => $params,
            ])
            ->execute();
    }

    /**
     * update item in the database using prepared statements
     * @param $params
     * @throws \yii\db\Exception
     */
    public function update($params) {
        $connection = Yii::$app->db;

        $connection->createCommand(
            "update addressbook set first_name=:first_name, last_name=:last_name, dob=:dob, zip=:zip where id=:id"
        )
        ->bindValues([
            ':first_name' => $params['fname'],
            ':last_name' => $params['lname'],
            ':dob' => $this->dateFormat($params['dob']),
            ':zip' => $params['zip'],
            ':id' => $params['id'],
        ])
        ->execute();
    }

    /**
     * get all stored data
     * @return array
     */
    public function listAll() {
        return (new Query())->select('id, first_name, last_name, dob, zip')
            ->from('addressbook')
            ->createCommand()->queryAll();
    }

    /**
     * get item from database and format the date
     * @param $id
     * @return array|bool
     */
    public function listOne($id) {
        $data = (new Query())->select('id, first_name, last_name, dob, zip')
            ->from('addressbook')
            ->where('id=:id', [':id' => $id])
            ->createCommand()->queryOne();

        if (empty($data)) {
            return $data;
        }

        $data['dob'] = (new \DateTime($data['dob']))->format('m/d/Y');

        return $data;
    }
}
