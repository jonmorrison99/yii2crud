<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('addressFormSubmitted')): ?>

        <div class="alert alert-success">
            Please enter the details below:
        </div>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['action' => '/site/add', 'id' => 'contact-form', 'options' => ['method' => 'post']]); ?>
                    <?= $form->field($model, 'fname')->label('First name:'); ?>
                    <?= $form->field($model, 'lname')->label('Last name:'); ?>
                    <?= $form->field($model, 'dob')->label('Date of Birth:')->textInput(['placeholder' => '12/14/2015']); ?>
                    <?= $form->field($model, 'zip')->label('Zip:'); ?>

                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'contact-button']); ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
