<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <?php
        if (sizeof($this->params) == 0) :
            echo "<div class=\"alert alert-success\" role=\"alert\">You have no entries in your address book. Click the create button to get started: " . Html::a('Create +', ['/site/new'], ['class'=>'btn btn-primary']) . "</div>";

        else :
        ?>
        <div class="row col-lg-12">
            <?= Html::a('Create +', ['/site/new'], ['class'=>'btn btn-primary']) ?>
        </div>
        <div class="row col-lg-12">
            <table class="table">
                <tr>
                    <th>First</th>
                    <th>Last</th>
                    <th>D of B</th>
                    <th>Zip</th>
                    <th>&nbsp;</th>
                </tr>
                <?php
                foreach($this->params['addressBook'] as $item) {
                    echo "<tr>";
                    echo "<td>".(strlen($item['first_name']) ? Html::a($item['first_name'], ['/site/view/'.$item['id']]) : '&nbsp;')."</td>";
                    echo "<td>".(strlen($item['last_name']) ? $item['last_name'] : '&nbsp;')."</td>";
                    echo "<td>".(strlen($item['dob']) ? $item['dob'] : '&nbsp;')."</td>";
                    echo "<td>".(strlen($item['zip']) ? $item['zip'] : '&nbsp;')."</td>";
                    echo "<td>".Html::a('delete', ['/site/delete/'.$item['id']], ['class'=>'btn btn-danger btn-xs'])."</td>";
                    echo "</tr>";
                }
                ?>
            </table>
        </div>
        <div class="row col-lg-12">
            <?= Html::a('Create +', ['/site/new'], ['class'=>'btn btn-primary']) ?>
        </div>
        <?php endif; ?>
    </div>
</div>
