<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Saved new contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo Html::a('Create +', ['/site/new'], ['class'=>'btn btn-primary']) ?>
</div>
