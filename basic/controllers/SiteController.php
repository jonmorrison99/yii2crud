<?php

namespace app\controllers;

use app\models\AddressForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * list all of the data stored
     * @return string
     */
    public function actionIndex() {
        $data = (new AddressForm())->listAll();

        yii::$app->view->params['addressBook'] = $data;

        return $this->render('index');
    }

    /**
     * display form for creating and item
     * @return string
     */
    public function actionNew() {
        return $this->render('create', [
            'model' => (new AddressForm()),
        ]);
    }

    /**
     * get item from database, if not item, display error
     * @return string
     */
    public function actionView() {
        $params = Yii::$app->request->getQueryParams();

        if (!isset($params['id'])) {
            return $this->render('error');
        }

        $data = (new AddressForm())->listOne((int)$params['id']);

        if (empty($data)) {
            Yii::$app->getSession()->setFlash('message', 'No data.');
            return $this->render('error');
        }

        yii::$app->view->params = $data;

        return $this->render('view', [
            'model' => (new AddressForm()),
        ]);
    }

    /**
     * validate and insert
     * @return string
     */
    public function actionAdd() {
        $model = new \app\models\AddressForm();

        if (!Yii::$app->request->post()) {
            return $this->goBack();
        }

        $params = Yii::$app->request->post();
        $model->attributes = $params['AddressForm'];

        if ($model->validate()) {
            (new AddressForm())->save($model->attributes);
            return $this->render('saved');
        }
        else {
            return $this->render('create', [
                'model' => $model,
                'errors' => $model->errors,
            ]);
        }
    }


    /**
     * validate and update
     * @return string
     */
/*
    public function actionUpdate() {
        if (!Yii::$app->request->post()) {
            return $this->goBack();
        }

        $params = Yii::$app->request->post();

        (new AddressForm())->update($params['AddressForm']);

        return $this->render('updated');
    }
*/

    public function actionUpdate() {
        $model = new \app\models\AddressForm();

        if (!Yii::$app->request->post()) {
            return $this->goBack();
        }

        $params = Yii::$app->request->post();
        $model->attributes = $params['AddressForm'];

        if ($model->validate()) {
            (new AddressForm())->update($params['AddressForm']);
            return $this->render('updated');
        }
        else {
            $data = [
                'id' => $params['AddressForm']['id'],
                'first_name' => $params['AddressForm']['fname'],
                'last_name' => $params['AddressForm']['lname'],
                'dob' => $params['AddressForm']['dob'],
                'zip' => $params['AddressForm']['zip'],
            ];

            yii::$app->view->params = $data;


            return $this->render('view', [
                'model' => $model,
                'errors' => $model->errors,
            ]);
        }
    }


    public function actionDelete() {
        $params = Yii::$app->request->getQueryParams();

        if (!isset($params['id'])) {
            return $this->render('error');
        }

        (new AddressForm())->deleteOne($params['id']);

        return $this->redirect('/',302);
    }
}
