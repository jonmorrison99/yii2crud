# README #

### What is this repository for? ###

* This repo was a learning project as part of a job application for a Yii position
* Version v1

### How do I get set up? ###
* Clone Repo
* Run composer global require "fxp/composer-asset-plugin:~1.1.1"
* Run composer update
* Create a config/db.php file:

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=your-db-server;dbname=your-db-name',
    'username' => 'your-user',
    'password' => 'your-password',
    'charset' => 'utf8',
];


* Create DB and 'addressbook' table with the following schema

id => int (auto) not null
first_name => varchar(30) not null
last_name => varchar(30) not null
dob => date not null
zip => char(5) not null


### Contribution guidelines ###

* Feel free to contribute. I had a little trouble getting the client side form validation working and a didn't look at the server side validation, as it wasn't requested.

### Who do I talk to? ###

* Lionel Morrison (lionel@morrison.mobi)